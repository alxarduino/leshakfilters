/* src: https://bitbucket.org/alxarduino/leshakfilters */
#include "AvgFilter.h"

/******************************************  AvgFilterBase ***********************************/

AvgFilterBase::AvgFilterBase(unsigned int averageFactor){
   isReady=false;
  _averageFactor=averageFactor;
}

int AvgFilterBase::process(int measureValue){
  registerValue(measureValue);
  return currentValue;
}

int AvgFilterBase::getCurrentValue(){
  return currentValue;
}
  
void AvgFilterBase::init(int initValue){
  currentValue=initValue;
  isReady=true;
}  

/******************************************  AvgFilterSimple *********************************/


AvgFilterSimple::AvgFilterSimple(unsigned int averageFactor):AvgFilterBase(averageFactor){}

void AvgFilterSimple::registerValue(int measureValue){
  if(!isReady)init(measureValue) ; // set current value on first call
  else currentValue=_averageFactor? (currentValue * ((_averageFactor - 1)) + measureValue) / (_averageFactor):currentValue;
}



/******************************************  AvgFilter ***********************************/
AvgFilter::AvgFilter(unsigned int averageFactor):AvgFilterBase(averageFactor){
  frame=(int*) malloc(averageFactor * sizeof(int)); // allocate memory for samples
}

void AvgFilter::init(int initValue){
   AvgFilterBase::init(initValue);
   
   // fill frame
   for(unsigned int i=0;i<_averageFactor;i++){
      frame[i]=initValue;
   }
}

int AvgFilter::shiftFrameLeft(){
    int lostElement=frame[0]; 
    for(unsigned int i=0;i<_averageFactor-1;i++)frame[i]=frame[i+1];
    return lostElement;
}

void AvgFilter::registerValue(int measureValue){
  if(!isReady)init(measureValue) ; // set current value on first call
  else {
    int olderSample=shiftFrameLeft(); // remove older sample
    frame[_averageFactor-1]=measureValue; // save new value to right of frame;
    
    // recalculate average
    currentValue=currentValue -  (olderSample/_averageFactor) + (measureValue/_averageFactor);
  }
}

  



/* src: https://bitbucket.org/alxarduino/leshakfilters */

#include "MedianFilter.h"

int inputValues[]={6, 4, 4, 3, 2, 1, 99, 5, 6, 7, 8, 20, 21, 22, 23, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 40, 40, 40, 40};
#define COUNT (sizeof(inputValues)/sizeof(int))

int results[COUNT];
int expectedResults[]={6, 6, 6, 4, 4, 3, 3, 3, 5, 6, 7, 7, 8, 20, 21, 22, 23, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 40, 40};

MedianFilter filter(5); // медиану будем искать по пяти замерам


void setup(){
  
  Serial.begin(57600);
  Serial.println("Test started");
  

  // action
  for(int i=0;i<COUNT;i++){
    results[i]=filter.process(inputValues[i]);
  }

  
  // asserts
  checkResults();
  

}

void loop(){

}

void checkResults(){
  for(int i=0;i<COUNT;i++){
    if(results[i]!=expectedResults[i]){
        Serial.print("FAIL test!!!! At index=");
        Serial.println(i);
        return;
    }
  }  
  
   Serial.println("Test OK");
}







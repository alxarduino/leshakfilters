/* src: https://bitbucket.org/alxarduino/leshakfilters */

#include "AvgFilter.h"

AvgFilterSimple simpleAvgFilter(5); // calculate avg by 5 samples
AvgFilter realAvgFilter(5); // calculate avg by 5 samples

int inputValues[]={5, 10, 15 , 20 , 25,30,35};

// 5,5,5,5,5=25/5=5
// 5,5,5,5,10=30/5=6
// 5,5,5,10,15=40/5=8
// 5,5,10,15,20=55/5=11
// 5,10,15,20,25=75/5=15
// 10,15,20,25,30=100/5=15
// 15,20,25,30,35=125/5=25



#define COUNT (sizeof(inputValues)/sizeof(int))

int simpleResults[COUNT];
int realResults[COUNT];
int expectedResults[]={5,6,8,11,15,20,25};



void setup(){
  Serial.begin(57600);
  Serial.println("AVG Test");
  
    // filtering input[]
  for(int i=0;i<COUNT;i++){
    simpleResults[i]=simpleAvgFilter.process(inputValues[i]);
    realResults[i]=realAvgFilter.process(inputValues[i]);
  }

  

  
   printArray("         Input",inputValues,COUNT);
   printArray("FilteredSimple",simpleResults,COUNT);
   printArray("  FilteredReal",realResults,COUNT);
   printArray("      Expected",expectedResults,COUNT);
  
}

void loop(){
}


void printArray(char* arrayName,int data[],int length){
  Serial.print(arrayName);
  Serial.print("=[");
  for(int i=0;i<length;i++){
    Serial.print(data[i],DEC);
    
    if(i!=length-1)Serial.print(", ");
    else Serial.println("]");
  }
}

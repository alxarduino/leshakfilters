/* src: https://bitbucket.org/alxarduino/leshakfilters */

#include "MedianFilter.h"
#include "AvgFilter.h" 

#define PAUSE 500
#define INPUT_PIN A0



// filters
MedianFilter medianFilter(5); 
AvgFilterSimple simpleAvgFilter(5); 
AvgFilter realAvgFilter(5); 



void setup(){
   Serial.begin(57600);
   Serial.print("All Filters");
}

void loop(){
  int raw=analogRead(INPUT_PIN); 
  
  Serial.print("Time=");
  Serial.println(millis());
  
  Serial.print("Raw=");
  Serial.println(raw,DEC);
  
  Serial.print("Median=");
  Serial.println(medianFilter.process(raw),DEC);
  
  Serial.print("SimpleAvg=");
  Serial.println(simpleAvgFilter.process(raw),DEC);
  
   Serial.print("RealAvg=");
  Serial.println(realAvgFilter.process(raw),DEC);
  
  Serial.println();

  
  delay(500);
}

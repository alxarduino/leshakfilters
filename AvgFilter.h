/* src: https://bitbucket.org/alxarduino/leshakfilters */

#ifndef AVG_FILTER_H
#define AVG_FILTER_H 

#include "Arduino.h"

/*abstract */class AvgFilterBase{ 
   public:
     AvgFilterBase(unsigned int averageFactor);  
     int process(int measureValue);

     int getCurrentValue();
     
     virtual void init(int initValue);
     
     /* abstract */     
     virtual void registerValue(int measureValue);

      
    protected:
        boolean isReady; 
        double currentValue;
        unsigned int _averageFactor;
};

/* ================== AvgFilterSimple ====================*/

class AvgFilterSimple: public AvgFilterBase{
    public:
       AvgFilterSimple(unsigned int averageFactor);
     
      /* owerride */   
      void registerValue(int measureValue); 
    
};

/* ================== AvgFilterBase ====================*/

class AvgFilter: public AvgFilterBase{
    public:
       AvgFilter(unsigned int averageFactor);
     
      /* owerride */   
      void registerValue(int measureValue); 
      void init(int initValue);
      
    private:
      int* frame;
      int shiftFrameLeft();  // return losted elem (frame[0])
    
};




#endif
